import React, { Component } from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom'
import "./App.css";

import Cards from "./components/Cards.component";
import Detail from "./components/Detail.component";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container mt-3">
            <Route exact path={["/", "/pokemons"]} component={Cards} />
            <Route path="/:id" component={Detail} />
        </div>
      </Router>
    );
  }
}

export default App;