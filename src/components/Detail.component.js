import React, { Component } from "react";
import { connect } from "react-redux";
import { getDetail } from "../actions/detail";

class Detail extends Component {
    componentDidMount() {
        this.props.getDetail(this.props.match.params);
    }
    
    render() {
        const { detail } = this.props;
        const id = this.props.match.params.id;
        // console.log("detail component", detail.details.abilities);
        return (
            <div class="container">
            <div class="card-product">
                <div class="container-fliud">
                    <div class="wrapper row">
                        <div class="preview col-md-6">
                            <div class="preview-pic tab-content">
                                <div class="tab-pane active" id="pic-1"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`} /></div>
                                <div class="tab-pane" id="pic-2"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${id}.png`} /></div>
                                <div class="tab-pane" id="pic-3"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/${id}.png`} /></div>
                                <div class="tab-pane" id="pic-4"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`} /></div>
                            </div>
                            <ul class="preview-thumbnail nav nav-tabs">
                                <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`} /></a></li>
                                <li><a data-target="#pic-2" data-toggle="tab"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${id}.png`} /></a></li>
                                <li><a data-target="#pic-3" data-toggle="tab"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/${id}.png`} /></a></li>
                                <li><a data-target="#pic-3" data-toggle="tab"><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`} /></a></li>
                            </ul>
                        </div>

                        <div class="details col-md-6">
                            <h3 class="Name">{detail.details.name.toUpperCase()}</h3>
                            <p class="type"><span>Height: {detail.details.height}</span></p>
                            <p class="type"><span>Weight: {detail.details.weight}</span></p>
                            <p class="type"><span>Abilities: {detail.details.abilities.map((val) =>  {return <p> {val.ability.name} </p>} )}</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        detail: state.detail
    };
};

export default connect(mapStateToProps, { getDetail })(Detail);