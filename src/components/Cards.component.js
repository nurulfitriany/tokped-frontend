// create class
import React, { Component } from "react";
import { connect } from "react-redux";
import { getPokemons } from "../actions/pokemons";
// import { getImg } from "../actions/images";

class Cards extends Component {
    componentDidMount() {
        this.props.getPokemons();
    }

    render() {
        const pokemons = this.props;
        return (
            <div>
                <section>
                <h1>Pokemons</h1>
                
                {
                    pokemons.pokemons.pokemon.map(val => {
                        return <div id="poke_container"class="poke-container">
                            <div class="pokemon">                               
                                <div class="img-container">
                                    <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${val.url.slice(-3, -1)}.png`} alt="" />
                                </div>
                                <div class="info">
                                    <h3 class="name">{val.name}</h3> 
                                    <div class="number">
                                        <a class="btn" href={val.url.slice(-3, -1)}>
                                            Detail
                                        </a>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    })
                }
                    
                </section>
            </div>
        )
        
    }
};
const mapStateToProps = (state) => {
    return {
      pokemons: state.pokemons
    };  
  };

export default connect(mapStateToProps, { getPokemons })(Cards);