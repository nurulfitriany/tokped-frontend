import { 
    RETRIEVE_DETAIL_POKEMON_SUCCESS,
    RETRIEVE_DETAIL_POKEMON_STARTED,
    RETRIEVE_DETAIL_POKEMON_FAILURE,
} from "./actionTypes/detail";

import axios from 'axios';

export const getDetail = (id) => {
    return (dispatch, getState) => {
      dispatch(retrieveDetailStarted());
      console.log('current state get detail:', getState());
      console.log("id get detail", id.id)
      axios
        .get(`https://pokeapi.co/api/v2/pokemon/${id.id}/`, {
            headers: {
              "Content-type": "application/x-wwww-form-urlencoded",
              "Access-Control-Allow-Origin": "*",
            }
          })
        .then(res => {
          console.log("res", res)
          dispatch(retrieveDetailSuccess(res.data))
        })
        .catch(err => {
          dispatch(retrieveDetailFailure(err.message));
        });
      
    };
};

const retrieveDetailSuccess = pokemon => ({
    type: RETRIEVE_DETAIL_POKEMON_SUCCESS,
    payload: {
      ...pokemon
    }
  });
    
  const retrieveDetailStarted = () => ({
    type: RETRIEVE_DETAIL_POKEMON_STARTED
  });
  
  const retrieveDetailFailure = error => ({
    type: RETRIEVE_DETAIL_POKEMON_FAILURE,
    payload: {
      error
    }
  });