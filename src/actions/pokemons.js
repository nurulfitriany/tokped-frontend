import { 
    RETRIEVE_POKEMONS_SUCCESS,
    RETRIEVE_POKEMONS_STARTED,
    RETRIEVE_POKEMONS_FAILURE,
} from "./actionTypes/pokemons";

import axios from 'axios';

export const getPokemons = () => {
    return (dispatch, getState) => {
      dispatch(retrievePokemonsStarted());
      console.log('current state get pokemons:', getState());

      axios
        .get(`https://pokeapi.co/api/v2/pokemon?limit=100&offset=0`, {
            headers: {
              "Content-type": "application/x-wwww-form-urlencoded",
              "Access-Control-Allow-Origin": "*",
            }
          })
        .then(res => {
          dispatch(retrievePokemonsSuccess(res.data))
        })
        .catch(err => {
          dispatch(retrievePokemonsFailure(err.message));
        });
      
    };
};

const retrievePokemonsSuccess = pokemons => ({
  type: RETRIEVE_POKEMONS_SUCCESS,
  payload: {
    ...pokemons
  }
});
  
const retrievePokemonsStarted = () => ({
  type: RETRIEVE_POKEMONS_STARTED
});

const retrievePokemonsFailure = error => ({
  type: RETRIEVE_POKEMONS_FAILURE,
  payload: {
    error
  }
});


