import {
    RETRIEVE_DETAIL_POKEMON_SUCCESS,
    RETRIEVE_DETAIL_POKEMON_STARTED,
    RETRIEVE_DETAIL_POKEMON_FAILURE,
} from "../actions/actionTypes/detail";

const initialState = {
    loading: false,
    detail: [],
    error: null
};

function detailReducer(state = initialState, action) {
  console.log("action", action)
    switch (action.type) {
      case RETRIEVE_DETAIL_POKEMON_STARTED:
        return {
          ...state,
          loading: true
        };

      case RETRIEVE_DETAIL_POKEMON_SUCCESS:
        console.log('SUCCESS', action.payload)
        return {
          ...state,
          loading: false,
          error: null,
          details: action.payload,
        };
      
      case RETRIEVE_DETAIL_POKEMON_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
  
      default:
        console.log('default pokemon 2')
        return state;
    }
};
  
export default detailReducer;
