import {
    RETRIEVE_POKEMONS_SUCCESS,
    RETRIEVE_POKEMONS_STARTED,
    RETRIEVE_POKEMONS_FAILURE,
  } from "../actions/actionTypes/pokemons";
  
  const initialState = {
    loading: false,
    pokemon: [],
    error: null
  };
  
function pokemonReducer(state = initialState, action) {
    switch (action.type) {
      case RETRIEVE_POKEMONS_STARTED:
        return {
          ...state,
          loading: true
        };

      case RETRIEVE_POKEMONS_SUCCESS:
        console.log('SUCCESS', action.payload.results)

        return {
          ...state,
          loading: false,
          error: null,
          pokemon: action.payload.results,
        };
      
      case RETRIEVE_POKEMONS_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
  
      default:
        console.log('default pokemon 1')
        return state;
    }
};
  
export default pokemonReducer;