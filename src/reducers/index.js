import { combineReducers } from "redux";
import pokemons from "./pokemons";
import detail from "./detail";

export default combineReducers({
  pokemons,
  detail
});